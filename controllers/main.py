import functools
import logging
import uuid

import json

import werkzeug.urls
import werkzeug.utils
from werkzeug.exceptions import BadRequest

from odoo import tools, api, http, SUPERUSER_ID, _
from odoo.exceptions import AccessDenied
from odoo.http import request
from odoo import registry as registry_get

from odoo.addons.auth_oauth.controllers.main import OAuthLogin as Home
from odoo.addons.web.controllers.main import db_monodb, ensure_db, set_cookie_and_redirect, login_and_redirect


_logger = logging.getLogger(__name__)


#----------------------------------------------------------
# Controller
#----------------------------------------------------------
class IKOAuthLogin(Home):
    def list_providers(self):
        providers = super().list_providers()
        for provider_dict in providers:
            if provider_dict['id'] == request.env.ref('inouk_oauth.provider_azuread').id:
                provider_dict['nonce']=uuid.uuid4().hex
                provider_dict['prompt']='select_account'
                provider_dict['response_type']='token id_token'

        return providers

    @http.route()
    def web_login(self, *args, **kw):
        response = super().web_login(*args, **kw)
        if response.is_qweb:
            #response.qcontext['ik_enable_login_form'] = False
            #response.qcontext['disable_database_manager'] = True
            pass
        return response

    def _is_login_form_enabled(self):
        """ check wether login form allowing to log using internal email and 
        password is allowed.
        login form can be:
            * allowed globally 
            * disabled globally (inouk_oauth.login_form_disabled=True)
            *   in that case a set of IP from where login is enabled can be 
                specified using inouk_oauth.login_form_enabled_ips
        """
        get_param = request.env['ir.config_parameter'].sudo().get_param
        _is_login_form_disabled = get_param(
            'inouk_oauth.login_form_disabled', "False"
        ).lower() in ('1', 'true', 'yes')
        if _is_login_form_disabled:
            allowed_ips_raw = get_param('inouk_oauth.login_form_enabled_ips', '')
            allowed_ips = [allowed_ip.strip() for allowed_ip in allowed_ips_raw.split(',')]
            source_ip = request.httprequest.environ['REMOTE_ADDR']
            if source_ip in allowed_ips:
                return True
        return not _is_login_form_disabled

    def get_auth_signup_config(self):
        """retrieve the module config (which features are enabled) for the login page"""
        config_dict = super().get_auth_signup_config()
        get_param = request.env['ir.config_parameter'].sudo().get_param

        _login_form_enabled = self._is_login_form_enabled()
        config_dict['ik_login_form_enabled'] = _login_form_enabled

        # Disable Reset password if login form is not allowed
        # else use value from auth_signup.reset_password
        #if not _login_form_enabled:
        #    config_dict['reset_password_enabled'] = False

        # database manager access is set via cli config in web addon
        # But odoo.tools.config['list_db'] is checked only in /web/login controller
        # not /web/reset_password. So we evaluate odoo.tools.config['list_db'] here.
        # when enabled, we restrict it based on inouk_oauth.database_manager_enabled_ips
        config_dict['disable_database_manager'] = not tools.config['list_db']
        if not config_dict['disable_database_manager']:
            config_dict['disable_database_manager'] = True
            dbm_ips_raw = get_param('inouk_oauth.database_manager_enabled_ips', '')
            if dbm_ips_raw:
                dbm_ips = [allowed_ip.strip() for allowed_ip in dbm_ips_raw.split(',')]
                source_ip = request.httprequest.environ['REMOTE_ADDR']
                if source_ip in dbm_ips:
                    config_dict['disable_database_manager'] = False

        # signup is set by auth_signup addon from system param
        # and can be forced here.
        #config_dict['signup_enabled'] = True

        return config_dict

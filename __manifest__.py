##############################################################################
#
#    Inouk oauth
#    Copyright (c) 2021,2022  Cyril MORISSE (twitter: @cmorisse)
#
##############################################################################
{
    'name': 'Inouk oauth',
    'version': '1.0',
    'category': 'Inouk',
    'description': """
Add support for various oauth and OpenID Connect providers.
""",
    'author': "Cyril MORISSE (twitter: @cmorisse)",
    'license': 'OPL-1',
    #'website': '',
    'depends': [
        'auth_oauth', 'auth_signup'
    ],
    'data': [
        # 
        #'data/ir_config_parameter.xml',
        'data/auth_oauth_data.xml',

        #
        'assets_loader.xml',
        #'security/ir.model.access.csv',
        #'views/res_users_views.xml',
        #'wizards/res_users_wizard_views.xml',
        #'menu.xml',
        'views/webclient_templates.xml'
    ],
    'demo_xml': [],
    'installable': True,
    'application': True,
}
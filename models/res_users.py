import json
import logging

import requests
import werkzeug.urls
import werkzeug.utils


from odoo import api, fields, models
from odoo.exceptions import AccessDenied, UserError
from odoo.addons.auth_signup.models.res_users import SignupError
from odoo.addons.web.controllers.main import db_monodb, ensure_db, set_cookie_and_redirect, login_and_redirect

from odoo.addons import base
base.models.res_users.USER_PRIVATE_FIELDS.append('oauth_access_token')

class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.model
    def _auth_oauth_validate(self, provider, access_token):
        """ Overload to inject auth0 returned 'sub' value as expected 'user_id' """
        _result = super()._auth_oauth_validate(provider, access_token)
        if provider == self.env.ref("inouk_oauth.provider_auth0").id: 
            _result['user_id'] = _result['sub']            
        elif provider == self.env.ref("inouk_oauth.provider_azuread").id: 
            if not 'user_id' in _result:
                _uid = _result.get('sub', None)
                if not _uid:
                    _msg = "Failed to get user_id from Azure response: %s " % _result
                    _logger.error(_msg)
                else:
                    _result['user_id'] = _result['sub']    
        return _result

    def _auth_oauth_logout(self, session_token, redirect):
        """ Overload to inject auth0 returned 'sub' value as expected 'user_id' """
            
        if self.oauth_provider_id == self.env.ref("inouk_oauth.provider_auth0"):
            if self.oauth_access_token:
                self.sudo().oauth_access_token = None
                url = "%s/v2/logout?client_id=%s&returnTo=%s" % (
                    "https://dev-jn2shwza.eu.auth0.com", #self.oauth_provider_id.baseurl,
                    self.oauth_provider_id.client_id,
                    redirect
                )
                redirect = werkzeug.utils.redirect(url, 303)
                redirect.autocorrect_location_header = False
                return redirect
        return None

    @api.model
    def _generate_signup_values(self, provider, validation, params):
        values = super()._generate_signup_values(provider, validation, params)
        if provider == self.env.ref("inouk_oauth.provider_azuread").id: 
            values["login"] = values["email"]
            values["email"] = validation.get("userPrincipalName", values["email"])
            values["name"] = validation.get("displayName", values["email"])
        return values

    @api.model
    def _auth_oauth_rpc(self, endpoint, access_token):
        """ Overloaded to inject access token in header for Azure AD """
        if endpoint.startswith("https://graph.microsoft.com"):
            _resp = requests.get(
                endpoint, 
                headers={"Authorization": f"Bearer {access_token}"}
            ).json()
            return _resp
        return super()._auth_oauth_rpc(endpoint, access_token)

# inouk_core

# System Parameters

*inouk_oauth.login_form_disabled*: boolean

Define wether login form allows to log using internal email and password.

Login form can be:
    * allowed globally ; False Default
    * disabled globally ; True
        in that case a set of IP from where login is enabled can be specified using inouk_oauth.login_form_enabled_ips

*inouk_oauth.login_form_enabled_ips* ;    A list of IP allowed to login with username and password.
    inouk_oauth.login_form_enabled_ips = 89.2.224.145,109.190.128.136

*inouk_oauth.database_manager_enabled_ips* :  
    When database manager is enabled, this defineds a list of IP allowed to use it.
    eg. 	
    inouk_oauth.database_manager_enabled_ips=89.2.224.145,109.190.128.136